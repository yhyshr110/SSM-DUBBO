package com.yhy.serviceImpl;

import com.github.abel533.entity.Example;
import com.github.abel533.mapper.Mapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yhy.Exception.YhyException;
import com.yhy.interceptor.MethodCacheInterceptor;
import com.yhy.yhyProviderWeb.model.UserInfo;
import com.yhy.service.BaseService;
import com.yhy.service.yhyProviderWebService.UserInfoService;
import com.yhy.web.PropertyUtil;
import com.yhy.web.R;
import com.yhy.web.RedisUtil;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userInfoService")
public class UserServiceImpl extends BaseService<UserInfo> implements UserInfoService {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private Mapper<UserInfo> mapper;

    /**
     * 分页查询
     * @param page
     * @param rows
     * @return
     */
    @Override
    public R getAllInfos(String name,String startDate,String endDate,Integer page, Integer rows) throws YhyException{
        LOGGER.info("==========分页查询============");
        page = page==null?1:page;
        rows = rows==null?10:rows;
//        try {
//            int a = 1/0;
//        }catch (Exception e){
//            throw new YhyException(501,PropertyUtil.getString("501")+";  内部异常:"+e.getMessage());
//        }
//        UserInfo userInfo = new UserInfo();
//        userInfo.setName(name);
//        PageInfo<UserInfo> userInfoPageInfo = this.queryPageListByWhere(userInfo, page, rows);

        //分页
        PageHelper.startPage(page, rows);
        Example example= new Example(UserInfo.class);
        Example.Criteria criteria = example.createCriteria();
        if (null!=name && !name.equals("")){
            criteria.andEqualTo("name",name);
        }
        if(null!=startDate && !startDate.equals("") && null != endDate && !endDate.equals("")  ){
            criteria.andBetween("createDate",startDate,endDate);
        }
        List<UserInfo> list = mapper.selectByExample(example);
        PageInfo<UserInfo> userInfoPageInfo = new PageInfo<>(list);
        return R.ok().put("userInfoPageInfo",userInfoPageInfo);
    }


    /**
     * 根据手机号查询
     * @param phoneNum
     * @return
     */
    @Override
    public R selectByPhoneNum(String phoneNum) throws Exception{
        UserInfo userInfo = null;
        UserInfo param = new UserInfo();
        param.setPhoneNum(phoneNum);
//        try {
//            userInfo = this.queryOne(param);
//            int a = 1/0;
//        }catch (Exception e){
//            LOGGER.info(e.getMessage());
//            throw e;
//        }
        return R.ok().put("userInfo",userInfo);
    }

    /**
     * 根据手机号更新数据
     * @param phoneNum
     * @param name
     */
    @Override
    public R updateByPhoneNum(String phoneNum, String name) throws YhyException{
        UserInfo param = new UserInfo();
        param.setPhoneNum(phoneNum);
        UserInfo userInfo = this.queryOne(param);
        userInfo.setName(name);
        Integer inte = this.update(userInfo);
        Object[] a = new Object[]{phoneNum};
        String cacheKey = MethodCacheInterceptor.getCacheKey(this.getClass().getName(), "selectByPhoneNum", a);
        RedisUtil.remove(cacheKey);
        return R.error(200, PropertyUtil.getString("200"));

    }


}
