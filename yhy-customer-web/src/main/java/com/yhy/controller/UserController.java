package com.yhy.controller;

import com.yhy.Exception.YhyException;
import com.yhy.service.yhyProviderWebService.UserInfoService;
import com.yhy.web.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserInfoService userInfoService;

    @RequestMapping(value = "/getAllInfos", method = RequestMethod.GET)
    @ResponseBody
    public R getAllInfos (String name,
            String startDate,
            String endDate,
            Integer page,Integer rows
    )throws YhyException {
        return userInfoService.getAllInfos(name,startDate,endDate,page,rows);
    }

    @RequestMapping(value = "/getByPhoneNum", method = RequestMethod.GET)
    @ResponseBody
    public R getByPhoneNum(
    )throws Exception{
        return userInfoService.selectByPhoneNum("18621176862");
    }

    @RequestMapping(value = "/updateByPhoneNum", method = RequestMethod.POST)
    @ResponseBody
    public R updateByPhoneNum(
    )throws YhyException{
        return userInfoService.updateByPhoneNum("18621176862","刘hh");
    }
}
