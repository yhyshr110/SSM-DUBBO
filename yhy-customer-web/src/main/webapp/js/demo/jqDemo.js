$(function () {
    $("#jqGrid").jqGrid({
        url:'/user/getAllInfos',
        styleUI:'Bootstrap',
        datatype:"json",
        colModel:[
            {label:'编号',name:'id',key:true,width:75},
            {label:'昵称',name:'nickName',width:150},
            {label:'姓名',name:'name',width:75},
            {label:'类型',name:'cardType',width:75},
            {label:'卡号',name:'cardCode',width:150},
        ],
        viewrecords: true,
        height: 'auto',//高度自适应
        rowNum: 10,
        rowList: [10, 30, 50],
        rownumbers: true,
        rownumWidth: 45,
        autowidth: true,
        multiselect: true,//选择框
        pager: "#jqGridPager",
        jsonReader: {
            root: "userInfoPageInfo.list",
            page: "userInfoPageInfo.perPage",
            total: "userInfoPageInfo.pages",
            records: "userInfoPageInfo.total"
        },
        prmNames: {
            page: "page",
            rows: "rows",
            // order: "order"
        },



        gridComplete: function () {
            //隐藏grid底部滚动条
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({"overflow-x": "hidden"});
        }

    });



});

var vm = new Vue({
    el:'#userInfo',
    data:{
        list:true,
        addObj:false,
        name:'',
        startDate:'',
        endDate:'',
        addPra:{nickName:'',name:'',cardType:'',cardCode:''}
    },
    methods:{
        query:function () {
            this.reload();
        },
        reload:function (data) {
            $("#jqGrid").setGridParam({
                postData:{
                    name:this.name,
                    startDate : this.startDate,
                    endDate :this.endDate
                },
                page:1
            }).trigger("reloadGrid");

        },
        add:function () {
            this.list=false;
            this.addObj=true;
        },
        addSub:function () {
            this.list=true;
            this.addObj=false;
        },
        addCal:function () {
            this.addPra={nickName:'',name:'',cardType:'',cardCode:''};
        },
        edit:function () {

        },
        del:function () {

        }

    }

})
