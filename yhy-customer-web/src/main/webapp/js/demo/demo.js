$(function(){
    var vm=new Vue({
        el:'#hello',
        data:{
            id:'001',
            name:'汤姆克鲁斯',
            mobail:'13691172762',
            date:'2018-08-08',
            message:'hello Vue.js!',
            seen:false

        },
        methods:{
            getMessage:function () {
                return this.message+'学无止境';
            },
            show:function(seen){
                if(seen){
                    this.seen=false;
                }else{
                    this.seen=true;
                }
            }

        }
    })

    $("#list").jqGrid({
        url:'user/getAllInfos',
        datatype:'json',
        colNames:['id','姓名','手机号','日期'],
        colModel:[
            {name:'id',index:'id',width:55},
            {name:'name',index:'name',width:90},
            {name:'mobail',index:'phoneNum',width:100},
            {name:'date',index:'createDate',width:100}
        ],
        caption:"用户信息表",
    });

    var mm= new Vue({
        el:'#hello1',
        data:{
            class1:false,
            url:'http://localhost:8888/',
            input:'测试双向绑定'
        }
    })

    var mmm=new Vue({
        el:'#hello3',
        data:{
            OK:false
        },
        methods:{
            ifSwitch:function (OK) {
                if(OK){
                    this.OK=false;
                }else{
                    this.OK=true;
                }
            }
        }
    })

    var mmmm = new Vue({
        el:'#hello4',
        data:{
            map:[
                {id:'1',name:'小红',mobile:'13612345678',date:'2018-01-01'},
                {id:'2',name:'小红红',mobile:'13656781234',date:'2018-02-01'},
                {id:'3',name:'小小红',mobile:'13612563478',date:'2018-03-01'}
            ]
        }
    })

    var m5 = new Vue({
        el:'#hello5',
        data:{
            OK:false,
            count:0
        },
        methods:{
            okSwitch:function(OK){
                if(OK){
                    this.OK=false;
                }else{
                    this.OK=true;
                }
            }
        }
    })



})