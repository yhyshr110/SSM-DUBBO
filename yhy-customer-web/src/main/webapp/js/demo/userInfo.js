

    var userArrJson = [
        {'id': 'stu0001', 'name': '张三', 'phoneNum': 85, 'createDate': 90},
        {'id': 'stu0002', 'name': '李四', 'phoneNum': 88, 'createDate': 85},
        {'id': 'stu0003', 'name': '王五', 'phoneNum': 65, 'createDate': 75},
        {'id': 'stu0004', 'name': '刘六', 'phoneNum': 58, 'createDate': 96}
    ];
    var vm = new Vue({
        el: '#userInfo',
        data: {
            userArr: [
                {'id': 'stu0001', 'name': '张三', 'phoneNum': 85, 'createDate': 90},
                {'id': 'stu0002', 'name': '李四', 'phoneNum': 88, 'createDate': 85},
                {'id': 'stu0003', 'name': '王五', 'phoneNum': 65, 'createDate': 75},
                {'id': 'stu0004', 'name': '刘六', 'phoneNum': 58, 'createDate': 96}
            ],
            addArr: {'name': '', 'phoneNum': '', 'password': ''},
            nowEditCol: -1,//当前编辑的行
            editStatus: false,//当前是否在编辑状态
            searchTxt: '',//搜索字段
            sortKey: 'createDate',//排序字段
            sortClass: '1'//排序类型
        },
        methods: {
            //启动index数据编辑
            startEdit: function (id) {
                this.nowEditCol = id;
            },
            cancelEdit: function () {
                this.nowEditCol = -1;
            },
            //修改确认
            sureEdit: function (id) {
                for (var i = 0; i < this.userArr.length; i++) {
                    if (id === this.userArr[i]['id']) {
                        this.userArr.splice(i, 1, this.editArr);
                        break;
                    }
                }
                this.nowEditCol = -1;
            },
            //删除数据
            deleteUser: function (id) {
                for (var i = 0; i < this.userArr.length; i++) {
                    if (id === this.userArr[i]['id']) {
                        this.userArr.splice(i, 1);
                        break;
                    }
                }
            },
            //新增数据
            submitUser: function () {
                var addArr = {
                    'name': this.addArr.name,
                    'phoneNum': this.addArr.phoneNum,
                    'password': this.addArr.password
                };
                this.userArr.push(addArr);
                this.resetUser();
            },
            //重置新增表单
            resetUser: function () {
                this.addArr = {'name': '', 'phoneNum': '', 'password': ''};
            },
            //查询
            query:function () {
                this.reload();
            },
            reload:function(searchTxt){
                var page = $("#userList").jqGrid('getGridParam','page')
            }
        },
        computed: {
            //存储当前编辑的对象
            editArr: function () {
                var editO = {};
                for (var i = 0; i < this.userArr.length; i++) {
                    if (this.nowEditCol === this.userArr[i]['id']) {
                        editO = this.userArr[i];
                        break;
                    }
                }
                return {
                    'id': editO.id,
                    'name': editO.name,
                    'phoneNum': editO.phoneNum,
                    'createDate': editO.createDate
                }
            }
        },
        


    })
