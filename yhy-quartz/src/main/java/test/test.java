package test;

import com.yhy.job.HelloWorldJob;
import com.yhy.job.NihaoJob;
import com.yhy.service.quartzService.IquartzService;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class test {

    public static void main(String[] args) throws BeansException {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext_provider.xml");
        IquartzService quartzManager = (IquartzService) ctx.getBean("iquartzService");
        test02(quartzManager);
//        test01();

    }

    public static void test01(){
        try {
            System.out.println("【系统启动】开始(每1秒输出一次)...");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void test02(IquartzService quartzManager){
        try {
            System.out.println("【系统启动】开始(每1秒输出一次 MyJob2)...");

            Thread.sleep(5000);
            System.out.println("【增加hellowJob启动】开始(每1秒输出一次)...");
            quartzManager.addJob("test", "test", "test", "test", HelloWorldJob.class, "0/1 * * * * ?");
            System.out.println("【增加你好启动】开始(每1秒输出一次)...");
            quartzManager.addJob("test1", "test1", "test1", "test1", NihaoJob.class, "0/1 * * * * ?");

            Thread.sleep(5000);
            System.out.println("【修改hellowJob时间】开始(每2秒输出一次)...");
            quartzManager.modifyJobTime("test", "test", "test", "test", "0/2 * * * * ?");

            Thread.sleep(10000);
            System.out.println("【移除hellowJob定时】开始...");
            quartzManager.removeJob("test", "test", "test", "test");

            // 关掉任务调度容器
            Thread.sleep(5000);
            quartzManager.shutdownJobs();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


