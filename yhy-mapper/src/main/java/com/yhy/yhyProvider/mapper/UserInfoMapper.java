package com.yhy.yhyProvider.mapper;

import com.github.abel533.mapper.Mapper;
import com.github.pagehelper.PageInfo;
import com.yhy.yhyProviderWeb.model.UserInfo;
import org.springframework.stereotype.Repository;
@Repository
public interface UserInfoMapper extends Mapper<UserInfo> {

    /**
     * 查询所有信息
     * @return
     */
    PageInfo<UserInfo> getAllInfos();
}