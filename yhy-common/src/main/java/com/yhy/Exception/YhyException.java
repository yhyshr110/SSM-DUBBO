package com.yhy.Exception;

/**
 * 自定义的异常处理类,针对预期的异常,需在程序中抛出此类异常
 */
public class YhyException extends Exception {

    //异常信息
    private String meg;

    //异常代码
    private Integer code;

    //构造器
    public YhyException(Integer code,String meg){
        super(meg);
        this.meg=meg;
        this.code=code;
    }

    public String getMeg() {
        return meg;
    }

    public void setMeg(String meg) {
        this.meg = meg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
