package com.yhy.Exception;

import com.alibaba.fastjson.JSONObject;
import com.yhy.web.R;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * 全局异常处理器
 */
public class YhyExceptionResolver implements HandlerExceptionResolver{

    /**
     * 系统抛出的异常处理
     * @param request
     * @param response
     * @param handler  就是处理器适配器要执行的Handler对象(只有method)
     * @param ex
     * @return
     */
    @Override
    @ExceptionHandler
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {

        YhyException yhyException = null;

        //如果该 异常类型是自定义的异常，直接取出异常信息，在错误页面展示。
        if(ex instanceof YhyException){
            yhyException = (YhyException)ex;
        }else{
            //如果该 异常类型不是自定义的异常，构造一个自定义的异常类型（信息为“未知错误”）。
            yhyException=new YhyException(500,ex.getMessage());
        }
        //错误信息
        printWrite(yhyException, response);
        ex.printStackTrace();
        return new ModelAndView();
    }

    /**
     * 将错误信息添加到response中
     *
     * @param yhyException
     * @param response
     */
    public static void printWrite(YhyException yhyException, HttpServletResponse response) {
        try {

            PrintWriter out = response.getWriter();
            out.write(JSONObject.toJSONString(R.error(yhyException.getCode(),yhyException.getMeg())));
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
