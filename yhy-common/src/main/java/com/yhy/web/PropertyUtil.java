package com.yhy.web;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.math.BigDecimal;
import java.util.*;


/**
 * 属性工具类
 *
 */
public class PropertyUtil extends PropertyPlaceholderConfigurer{

    private static Map<String, String> ctxPropertiesMap;

    @Override
    protected void processProperties(
            ConfigurableListableBeanFactory beanFactoryToProcess,
            Properties props) throws BeansException {
        super.processProperties(beanFactoryToProcess, props);
        ctxPropertiesMap = new HashMap<String, String>();
        for (Object key : props.keySet()) {
            String keyStr = key.toString();
            String value = props.getProperty(keyStr);
            ctxPropertiesMap.put(keyStr, value);
        }
    }

    public static String getString(String key) {
        if(ctxPropertiesMap == null) {
            ctxPropertiesMap = new HashMap<String, String>();
        }
        return ctxPropertiesMap.get(key);
    }

    public static Integer getInteger(String key) {
        if(ctxPropertiesMap == null) {
            ctxPropertiesMap = new HashMap<String, String>();
        }
        String obj=ctxPropertiesMap.get(key);
        if(obj!=null){
            return Integer.valueOf(obj);
        }
        return null;
    }

    public static Double getDouble(String key) {
        if(ctxPropertiesMap == null) {
            ctxPropertiesMap = new HashMap<String, String>();
        }
        String obj=ctxPropertiesMap.get(key);
        if(obj!=null){
            return Double.valueOf(obj);
        }
        return null;
    }

    public static BigDecimal getBigDecimal(String key) {
        if(ctxPropertiesMap == null) {
            ctxPropertiesMap = new HashMap<String, String>();
        }
        String obj=ctxPropertiesMap.get(key);
        if(obj!=null){
            return new BigDecimal(obj);
        }
        return null;
    }

    /**
     * 根据键值获取Property文件中的值
     * @author 吴鹏
     * @param key 键值
     * @param defaultValue 默认值
     * @return 根据键值取得的结构
     */
    public static String getString(String key, String defaultValue){
        if(ctxPropertiesMap == null) {
            ctxPropertiesMap = new HashMap<String, String>();
        }

        String value = ctxPropertiesMap.get(key);

        return (value == null) ? defaultValue : value ;
    }

    public static int getIntValue(String key, int defaultValue) {
        try {
            int r = Integer.parseInt(getString(key));
            return r;
        } catch (Exception e) {
            return defaultValue;
        }
    }

    /**
     * 取出复合表达式的keys
     * @param express
     * @return
     */
    public static Set<String> getkeys(String express){
        Set<String> selectedkey = new HashSet<String>();
        Set<String> keys =  ctxPropertiesMap.keySet();
        for(String item : keys){
            if(item.matches(express)){
                selectedkey.add(item);
            }
        }
        return selectedkey;
    }

    /**
     * 取工商局标志样式
     * @return
     */
    public static String getGsjIcon(){
        String flg = getString("gsjIcon.isShow");
        if("true".equals(flg)){
            return "GSJIcon";
        }else{
            return "";
        }
    }
}

