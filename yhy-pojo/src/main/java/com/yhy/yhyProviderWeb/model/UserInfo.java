package com.yhy.yhyProviderWeb.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
@Table
public class UserInfo implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String nickName;

    private String name;

    private Byte cardType;

    private String cardCode;

    private String phoneNum;

    private String password;

    private String bankCode;

    private String bankCardCode;

    private String bankName;

    private String bankBranchName;

    private Integer refereeId;

    private Integer channel;

    private Byte clientType;

    private String device;

    private String loginFlag;

    private Byte certificationFlag;

    private Byte customType;

    private Byte loanFlag;

    private String userImg;

    private Byte isVip;

    private String vipName;

    private Integer vipLevel;

    private Date vipAuthTime;

    private Date vipExpire;

    private Date createDate;

    private Integer assetId;

    private String optIp;

    private String mobile;

    private String address;

    private String updateTime;

    private Byte tender;

    private Byte repayment;

    private Byte creditAssignment;

    private Byte withdraw;

    private Byte recharge;

    private Byte compensatory;

    private Byte isInvest;

    private Byte isGuaranteecorp;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName == null ? null : nickName.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Byte getCardType() {
        return cardType;
    }

    public void setCardType(Byte cardType) {
        this.cardType = cardType;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode == null ? null : cardCode.trim();
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum == null ? null : phoneNum.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode == null ? null : bankCode.trim();
    }

    public String getBankCardCode() {
        return bankCardCode;
    }

    public void setBankCardCode(String bankCardCode) {
        this.bankCardCode = bankCardCode == null ? null : bankCardCode.trim();
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName == null ? null : bankName.trim();
    }

    public String getBankBranchName() {
        return bankBranchName;
    }

    public void setBankBranchName(String bankBranchName) {
        this.bankBranchName = bankBranchName == null ? null : bankBranchName.trim();
    }

    public Integer getRefereeId() {
        return refereeId;
    }

    public void setRefereeId(Integer refereeId) {
        this.refereeId = refereeId;
    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public Byte getClientType() {
        return clientType;
    }

    public void setClientType(Byte clientType) {
        this.clientType = clientType;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device == null ? null : device.trim();
    }

    public String getLoginFlag() {
        return loginFlag;
    }

    public void setLoginFlag(String loginFlag) {
        this.loginFlag = loginFlag == null ? null : loginFlag.trim();
    }

    public Byte getCertificationFlag() {
        return certificationFlag;
    }

    public void setCertificationFlag(Byte certificationFlag) {
        this.certificationFlag = certificationFlag;
    }

    public Byte getCustomType() {
        return customType;
    }

    public void setCustomType(Byte customType) {
        this.customType = customType;
    }

    public Byte getLoanFlag() {
        return loanFlag;
    }

    public void setLoanFlag(Byte loanFlag) {
        this.loanFlag = loanFlag;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg == null ? null : userImg.trim();
    }

    public Byte getIsVip() {
        return isVip;
    }

    public void setIsVip(Byte isVip) {
        this.isVip = isVip;
    }

    public String getVipName() {
        return vipName;
    }

    public void setVipName(String vipName) {
        this.vipName = vipName == null ? null : vipName.trim();
    }

    public Integer getVipLevel() {
        return vipLevel;
    }

    public void setVipLevel(Integer vipLevel) {
        this.vipLevel = vipLevel;
    }

    public Date getVipAuthTime() {
        return vipAuthTime;
    }

    public void setVipAuthTime(Date vipAuthTime) {
        this.vipAuthTime = vipAuthTime;
    }

    public Date getVipExpire() {
        return vipExpire;
    }

    public void setVipExpire(Date vipExpire) {
        this.vipExpire = vipExpire;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getAssetId() {
        return assetId;
    }

    public void setAssetId(Integer assetId) {
        this.assetId = assetId;
    }

    public String getOptIp() {
        return optIp;
    }

    public void setOptIp(String optIp) {
        this.optIp = optIp == null ? null : optIp.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }

    public Byte getTender() {
        return tender;
    }

    public void setTender(Byte tender) {
        this.tender = tender;
    }

    public Byte getRepayment() {
        return repayment;
    }

    public void setRepayment(Byte repayment) {
        this.repayment = repayment;
    }

    public Byte getCreditAssignment() {
        return creditAssignment;
    }

    public void setCreditAssignment(Byte creditAssignment) {
        this.creditAssignment = creditAssignment;
    }

    public Byte getWithdraw() {
        return withdraw;
    }

    public void setWithdraw(Byte withdraw) {
        this.withdraw = withdraw;
    }

    public Byte getRecharge() {
        return recharge;
    }

    public void setRecharge(Byte recharge) {
        this.recharge = recharge;
    }

    public Byte getCompensatory() {
        return compensatory;
    }

    public void setCompensatory(Byte compensatory) {
        this.compensatory = compensatory;
    }

    public Byte getIsInvest() {
        return isInvest;
    }

    public void setIsInvest(Byte isInvest) {
        this.isInvest = isInvest;
    }

    public Byte getIsGuaranteecorp() {
        return isGuaranteecorp;
    }

    public void setIsGuaranteecorp(Byte isGuaranteecorp) {
        this.isGuaranteecorp = isGuaranteecorp;
    }
}