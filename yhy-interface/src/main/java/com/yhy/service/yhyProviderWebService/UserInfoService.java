package com.yhy.service.yhyProviderWebService;


import com.yhy.Exception.YhyException;
import com.yhy.web.R;

public interface UserInfoService {

    /**
     * 获取所有信息
     * @param page
     * @param rows
     * @return
     */
    public R getAllInfos(String name,String startDate,String endDate,Integer page, Integer rows)throws YhyException;

    /**
     * 根据手机号查询
     * @param phoneNum
     * @return
     */
    R selectByPhoneNum(String phoneNum)throws Exception;

    /**
     * 根据手机号更新数据
     * @param phoneNum
     * @param name
     */
    R updateByPhoneNum(String phoneNum,String name)throws YhyException;
}
