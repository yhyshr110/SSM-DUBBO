package com.yhy.service.quartzService;

import org.quartz.*;

public interface IquartzService {

    public void addJob(String jobName, String jobGroupName,
                       String triggerName, String triggerGroupName, Class jobClass, String cron);


    /**
     * @Description: 修改一个任务的触发时间
     *
     * @param jobName
     * @param jobGroupName
     * @param triggerName 触发器名
     * @param triggerGroupName 触发器组名
     * @param cron   时间设置，参考quartz说明文档
     */
    public void modifyJobTime(String jobName,
                              String jobGroupName, String triggerName, String triggerGroupName, String cron);

    /**
     * @Description: 移除一个任务
     *
     * @param jobName
     * @param jobGroupName
     * @param triggerName
     * @param triggerGroupName
     */
    public void removeJob(String jobName, String jobGroupName,
                          String triggerName, String triggerGroupName);

    /**
     * @Description:启动所有定时任务
     */
    public void startJobs();

    /**
     * @Description:关闭所有定时任务
     */
    public void shutdownJobs();

    public Scheduler getScheduler();

    public void setScheduler(Scheduler scheduler);
}
